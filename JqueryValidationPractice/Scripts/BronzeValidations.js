﻿/**
 * Any Input Validation
 * 
 * Prerequisites: jQuery
 *
 * Supported Validation Classes:
 * |===============================|
 * | numberInput | number          |
 * |===============================|
 *
 * How to use:
 *
 * <div>
 *  <input class="numberInput"/> <!--replace numberInput with the validation you want to use-->
 *  <p class="validation-message"> Your validation message</p>
 * </div>
 * 
 */

(function () {
    var debug = true;
    var updateErrorCount = function() {
        var numberOfErrors = $(".has-error").length;
        $("input[type=hidden]#numberOfErrors").val(numberOfErrors);
    }

    var regexInputVal = function(element, pattern) {
        if (debug) console.log("Validate Regex pattern: " + pattern);
        if (!element.val().match(pattern)) {
            element.addClass("has-error");
            element.nextAll(".validation-message:first").show();
        } else {
            element.removeClass("has-error");
            element.nextAll(".validation-message:first").hide();
        }

        updateErrorCount();
    }

    var numberInputVal = function (element) {
        if (debug) console.log("Validate Number Format");
        regexInputVal(element, "^(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?$|^$");
    };

    /**
     * Number field validation
     *
     * Use case add class numberInput to the class you wish to use.
     * if you want to display a validation message, create a sibling tag with the class "validation-message" after the input you are validating.
     * 
     * for example:
     *
     * <div>
     *  <input class="numberInput"/>
     *  <p class="validation-message"> Enter a number</p>
     * </div>
     */
    $("input.numberInput").on("keyup",
        function() {
            numberInputVal($(this));
        });
    $("input.regexInput").on("keyup",
        function () {
            regexInputVal($(this), $(this).prop("pattern"));
        });
})();
